# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :group do
    trait :all do
      group_id { 'Best to not hard code this' }
      name { Faker::Lorem.paragraph_by_chars(number: 40) }
      developer_name { Faker::Lorem.paragraph_by_chars(number: 80) }
      # Please add Related to .leap_salesforce.yml (if it's a table) to create association for Related
      type { Group::Type.sample }
      email { 'Content depending on email' }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      send_email_to_members { true }
      include_bosses { true }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
    end
  end
end
