# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :attachment do
    trait :all do
      attachment_id { 'Best to not hard code this' }
      deleted { true }
      # Please add Parent to .leap_salesforce.yml (if it's a table) to create association for Parent
      file_name { Faker::Lorem.paragraph_by_chars(number: 255) }

      private { true }

      content_type { Faker::Lorem.paragraph_by_chars(number: 120) }
      body_length { 'Content depending on int' }
      body { 'Content depending on base64' }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      description { 'Content depending on textarea' }
    end
  end
end
