# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :contact do
    last_name { Faker::Lorem.paragraph_by_chars(number: 80) }
    first_name { Faker::Lorem.paragraph_by_chars(number: 40) }
    trait :account do
      association :account_id, factory: :account
    end
    trait :all do
      contact_id { 'Best to not hard code this' }
      deleted { true }
      # Please add MasterRecord to .leap_salesforce.yml (if it's a table) to create association for MasterRecord
      association :account_id, factory: :account
      salutation { Contact::Salutation.sample }
      full_name { Faker::Lorem.paragraph_by_chars(number: 121) }
      other_street { 'Content depending on textarea' }
      other_city { Faker::Lorem.paragraph_by_chars(number: 40) }
      other_state_province { Faker::Lorem.paragraph_by_chars(number: 80) }
      other_zip_postal_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      other_country { Faker::Lorem.paragraph_by_chars(number: 80) }
      mailing_city { Faker::Lorem.paragraph_by_chars(number: 40) }
      mailing_state_province { Faker::Lorem.paragraph_by_chars(number: 80) }
      mailing_zip_postal_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      mailing_country { Faker::Lorem.paragraph_by_chars(number: 80) }
      location_latitude { 'Content depending on double' }
      location_longitude { 'Content depending on double' }
      location { 'Content depending on location' }
    end
  end
end
