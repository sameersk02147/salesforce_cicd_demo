# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Document, PublishStatus
class Document < SoqlData
  # Enumeration for PublishStatus
  module PublishStatus
    include SoqlEnum

    @upload_interrupted = 'Upload Interrupted'

    @public = 'Public'

    @private_library = 'Private Library'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Publish Status'
      end

      # @return [Array] List of values for PublishStatus
      def values
        ['Upload Interrupted', 'Public', 'Private Library']
      end

      attr_reader :upload_interrupted, :public, :private_library
    end
  end
end
