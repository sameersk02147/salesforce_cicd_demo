# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Document, FilePrivacyonRecords
class Document < SoqlData
  # Enumeration for FilePrivacyonRecords
  module FilePrivacyonRecords
    include SoqlEnum

    @visible_to_anyone_with_record_access = 'Visible to Anyone With Record Access'

    @private_on_records = 'Private on Records'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'File Privacy on Records'
      end

      # @return [Array] List of values for FilePrivacyonRecords
      def values
        ['Visible to Anyone With Record Access', 'Private on Records']
      end

      attr_reader :visible_to_anyone_with_record_access, :private_on_records
    end
  end
end
