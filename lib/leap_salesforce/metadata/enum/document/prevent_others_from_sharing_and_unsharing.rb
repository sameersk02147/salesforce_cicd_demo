# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Document, Preventothersfromsharingandunsharing
class Document < SoqlData
  # Enumeration for Preventothersfromsharingandunsharing
  module Preventothersfromsharingandunsharing
    include SoqlEnum

    @freeze_sharing_off = 'Freeze Sharing Off'

    @freeze_sharing_on = 'Freeze Sharing On'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Prevent others from sharing and unsharing'
      end

      # @return [Array] List of values for Preventothersfromsharingandunsharing
      def values
        ['Freeze Sharing Off', 'Freeze Sharing On']
      end

      attr_reader :freeze_sharing_off, :freeze_sharing_on
    end
  end
end
