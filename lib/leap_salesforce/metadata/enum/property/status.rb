# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Property, Status
class Property < SoqlData
  # Enumeration for Status
  module Status
    include SoqlEnum

    @contracted = 'Contracted'

    @pre_market = 'Pre Market'

    @available = 'Available'

    @under_agreement = 'Under Agreement'

    @closed = 'Closed'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Status'
      end

      # @return [Array] List of values for Status
      def values
        ['Contracted', 'Pre Market', 'Available', 'Under Agreement', 'Closed']
      end

      attr_reader :contracted, :pre_market, :available, :under_agreement, :closed
    end
  end
end
