# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Case, CaseType
class Case < SoqlData
  # Enumeration for CaseType
  module CaseType
    include SoqlEnum

    @mechanical = 'Mechanical'

    @electrical = 'Electrical'

    @electronic = 'Electronic'

    @structural = 'Structural'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Case Type'
      end

      # @return [Array] List of values for CaseType
      def values
        %w[Mechanical Electrical Electronic Structural Other]
      end

      attr_reader :mechanical, :electrical, :electronic, :structural, :other
    end
  end
end
