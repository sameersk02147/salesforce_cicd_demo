# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Case, CaseOrigin
class Case < SoqlData
  # Enumeration for CaseOrigin
  module CaseOrigin
    include SoqlEnum

    @phone = 'Phone'

    @email = 'Email'

    @web = 'Web'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Case Origin'
      end

      # @return [Array] List of values for CaseOrigin
      def values
        %w[Phone Email Web]
      end

      attr_reader :phone, :email, :web
    end
  end
end
