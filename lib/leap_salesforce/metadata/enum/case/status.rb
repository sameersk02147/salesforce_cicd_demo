# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Case, Status
class Case < SoqlData
  # Enumeration for Status
  module Status
    include SoqlEnum

    @new = 'New'

    @working = 'Working'

    @escalated = 'Escalated'

    @closed = 'Closed'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Status'
      end

      # @return [Array] List of values for Status
      def values
        %w[New Working Escalated Closed]
      end

      attr_reader :new, :working, :escalated, :closed
    end
  end
end
