# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Case, CaseReason
class Case < SoqlData
  # Enumeration for CaseReason
  module CaseReason
    include SoqlEnum

    @installation = 'Installation'

    @equipment_complexity = 'Equipment Complexity'

    @performance = 'Performance'

    @breakdown = 'Breakdown'

    @equipment_design = 'Equipment Design'

    @feedback = 'Feedback'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Case Reason'
      end

      # @return [Array] List of values for CaseReason
      def values
        ['Installation', 'Equipment Complexity', 'Performance', 'Breakdown', 'Equipment Design', 'Feedback', 'Other']
      end

      attr_reader :installation, :equipment_complexity, :performance, :breakdown, :equipment_design, :feedback, :other
    end
  end
end
