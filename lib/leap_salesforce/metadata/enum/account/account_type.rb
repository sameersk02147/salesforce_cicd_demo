# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, AccountType
class Account < SoqlData
  # Enumeration for AccountType
  module AccountType
    include SoqlEnum

    @prospect = 'Prospect'

    @customer_direct = 'Customer - Direct'

    @customer_channel = 'Customer - Channel'

    @channel_partner_reseller = 'Channel Partner / Reseller'

    @installation_partner = 'Installation Partner'

    @technology_partner = 'Technology Partner'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Account Type'
      end

      # @return [Array] List of values for AccountType
      def values
        ['Prospect', 'Customer - Direct', 'Customer - Channel', 'Channel Partner / Reseller', 'Installation Partner', 'Technology Partner', 'Other']
      end

      attr_reader :prospect, :customer_direct, :customer_channel, :channel_partner_reseller, :installation_partner, :technology_partner, :other
    end
  end
end
