# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, AccountSource
class Account < SoqlData
  # Enumeration for AccountSource
  module AccountSource
    include SoqlEnum

    @web = 'Web'

    @phone_inquiry = 'Phone Inquiry'

    @partner_referral = 'Partner Referral'

    @purchased_list = 'Purchased List'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Account Source'
      end

      # @return [Array] List of values for AccountSource
      def values
        ['Web', 'Phone Inquiry', 'Partner Referral', 'Purchased List', 'Other']
      end

      attr_reader :web, :phone_inquiry, :partner_referral, :purchased_list, :other
    end
  end
end
