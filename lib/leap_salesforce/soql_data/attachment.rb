# frozen_string_literal: true

require_relative 'attachment_field_names'
# An Attachment object mapping to a SOQL Attachment
class Attachment < SoqlData
  include Attachment::Fields
end
