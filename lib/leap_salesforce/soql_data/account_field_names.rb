# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Account mapped from SOQL Account
class Account < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Account ID', type 'id'
    soql_element :account_id, 'Id'

    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'

    # Element for 'Master Record ID', type 'reference'
    soql_element :master_record_id, 'MasterRecordId'

    # Element for 'Account Name', type 'string'
    soql_element :account_name, 'Name'

    # Element for 'Account Type', type 'picklist'
    soql_element :account_type, 'Type'

    # Element for 'Parent Account ID', type 'reference'
    soql_element :parent_account_id, 'ParentId'

    # Element for 'Billing Street', type 'textarea'
    soql_element :billing_street, 'BillingStreet'

    # Element for 'Billing City', type 'string'
    soql_element :billing_city, 'BillingCity'

    # Element for 'Billing State/Province', type 'string'
    soql_element :billing_state_province, 'BillingState'

    # Element for 'Billing Zip/Postal Code', type 'string'
    soql_element :billing_zip_postal_code, 'BillingPostalCode'

    # Element for 'Billing Country', type 'string'
    soql_element :billing_country, 'BillingCountry'

    # Element for 'Billing Latitude', type 'double'
    soql_element :billing_latitude, 'BillingLatitude'

    # Element for 'Billing Longitude', type 'double'
    soql_element :billing_longitude, 'BillingLongitude'

    # Element for 'Billing Geocode Accuracy', type 'picklist'
    soql_element :billing_geocode_accuracy, 'BillingGeocodeAccuracy'

    # Element for 'Billing Address', type 'address'
    soql_element :billing_address, 'BillingAddress'

    # Element for 'Shipping Street', type 'textarea'
    soql_element :shipping_street, 'ShippingStreet'

    # Element for 'Shipping City', type 'string'
    soql_element :shipping_city, 'ShippingCity'

    # Element for 'Shipping State/Province', type 'string'
    soql_element :shipping_state_province, 'ShippingState'

    # Element for 'Shipping Zip/Postal Code', type 'string'
    soql_element :shipping_zip_postal_code, 'ShippingPostalCode'

    # Element for 'Shipping Country', type 'string'
    soql_element :shipping_country, 'ShippingCountry'

    # Element for 'Shipping Latitude', type 'double'
    soql_element :shipping_latitude, 'ShippingLatitude'

    # Element for 'Shipping Longitude', type 'double'
    soql_element :shipping_longitude, 'ShippingLongitude'

    # Element for 'Shipping Geocode Accuracy', type 'picklist'
    soql_element :shipping_geocode_accuracy, 'ShippingGeocodeAccuracy'

    # Element for 'Shipping Address', type 'address'
    soql_element :shipping_address, 'ShippingAddress'

    # Element for 'Account Phone', type 'phone'
    soql_element :account_phone, 'Phone'

    # Element for 'Account Fax', type 'phone'
    soql_element :account_fax, 'Fax'

    # Element for 'Account Number', type 'string'
    soql_element :account_number, 'AccountNumber'

    # Element for 'Website', type 'url'
    soql_element :website, 'Website'

    # Element for 'Photo URL', type 'url'
    soql_element :photo_url, 'PhotoUrl'

    # Element for 'SIC Code', type 'string'
    soql_element :sic_code, 'Sic'

    # Element for 'Industry', type 'picklist'
    soql_element :industry, 'Industry'

    # Element for 'Annual Revenue', type 'currency'
    soql_element :annual_revenue, 'AnnualRevenue'

    # Element for 'Employees', type 'int'
    soql_element :employees, 'NumberOfEmployees'

    # Element for 'Ownership', type 'picklist'
    soql_element :ownership, 'Ownership'

    # Element for 'Ticker Symbol', type 'string'
    soql_element :ticker_symbol, 'TickerSymbol'

    # Element for 'Account Description', type 'textarea'
    soql_element :account_description, 'Description'

    # Element for 'Account Rating', type 'picklist'
    soql_element :account_rating, 'Rating'

    # Element for 'Account Site', type 'string'
    soql_element :account_site, 'Site'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Last Activity', type 'date'
    soql_element :last_activity, 'LastActivityDate'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Data.com Key', type 'string'
    soql_element :datacom_key, 'Jigsaw'

    # Element for 'Jigsaw Company ID', type 'string'
    soql_element :jigsaw_company_id, 'JigsawCompanyId'

    # Element for 'Clean Status', type 'picklist'
    soql_element :clean_status, 'CleanStatus'

    # Element for 'Account Source', type 'picklist'
    soql_element :account_source, 'AccountSource'

    # Element for 'D-U-N-S Number', type 'string'
    soql_element :duns_number, 'DunsNumber'

    # Element for 'Tradestyle', type 'string'
    soql_element :tradestyle, 'Tradestyle'

    # Element for 'NAICS Code', type 'string'
    soql_element :naics_code, 'NaicsCode'

    # Element for 'NAICS Description', type 'string'
    soql_element :naics_description, 'NaicsDesc'

    # Element for 'Year Started', type 'string'
    soql_element :year_started, 'YearStarted'

    # Element for 'SIC Description', type 'string'
    soql_element :sic_description, 'SicDesc'

    # Element for 'D&B Company ID', type 'reference'
    soql_element :d_b_company_id, 'DandbCompanyId'

    # Element for 'Customer Priority', type 'picklist'
    soql_element :customer_priority, 'CustomerPriority__c'

    # Element for 'SLA', type 'picklist'
    soql_element :sla, 'SLA__c'

    # Element for 'Active', type 'picklist'
    soql_element :active, 'Active__c'

    # Element for 'Number of Locations', type 'double'
    soql_element :number_of_locations, 'NumberofLocations__c'

    # Element for 'Upsell Opportunity', type 'picklist'
    soql_element :upsell_opportunity, 'UpsellOpportunity__c'

    # Element for 'SLA Serial Number', type 'string'
    soql_element :sla_serial_number, 'SLASerialNumber__c'

    # Element for 'SLA Expiration Date', type 'date'
    soql_element :sla_expiration_date, 'SLAExpirationDate__c'
  end
end
