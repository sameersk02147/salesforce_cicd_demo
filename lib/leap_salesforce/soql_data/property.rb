# frozen_string_literal: true

require_relative 'property_field_names'
# An Property object mapping to a SOQL Property__c
class Property < SoqlData
  include Property::Fields
  soql_object 'Property__c'
end
