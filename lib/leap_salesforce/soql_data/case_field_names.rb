# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Case mapped from SOQL Case
class Case < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Case ID', type 'id'
    soql_element :case_id, 'Id'

    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'

    # Element for 'Case Number', type 'string'
    soql_element :case_number, 'CaseNumber'

    # Element for 'Contact ID', type 'reference'
    soql_element :contact_id, 'ContactId'

    # Element for 'Account ID', type 'reference'
    soql_element :account_id, 'AccountId'

    # Element for 'Asset ID', type 'reference'
    soql_element :asset_id, 'AssetId'

    # Element for 'Parent Case ID', type 'reference'
    soql_element :parent_case_id, 'ParentId'

    # Element for 'Name', type 'string'
    soql_element :name, 'SuppliedName'

    # Element for 'Email Address', type 'email'
    soql_element :email_address, 'SuppliedEmail'

    # Element for 'Phone', type 'string'
    soql_element :phone, 'SuppliedPhone'

    # Element for 'Company', type 'string'
    soql_element :company, 'SuppliedCompany'

    # Element for 'Case Type', type 'picklist'
    soql_element :case_type, 'Type'

    # Element for 'Status', type 'picklist'
    soql_element :status, 'Status'

    # Element for 'Case Reason', type 'picklist'
    soql_element :case_reason, 'Reason'

    # Element for 'Case Origin', type 'picklist'
    soql_element :case_origin, 'Origin'

    # Element for 'Subject', type 'string'
    soql_element :subject, 'Subject'

    # Element for 'Priority', type 'picklist'
    soql_element :priority, 'Priority'

    # Element for 'Description', type 'textarea'
    soql_element :description, 'Description'

    # Element for 'Closed', type 'boolean'
    soql_element :closed, 'IsClosed'

    # Element for 'Closed Date', type 'datetime'
    soql_element :closed_date, 'ClosedDate'

    # Element for 'Escalated', type 'boolean'
    soql_element :escalated, 'IsEscalated'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Contact Phone', type 'phone'
    soql_element :contact_phone, 'ContactPhone'

    # Element for 'Contact Mobile', type 'phone'
    soql_element :contact_mobile, 'ContactMobile'

    # Element for 'Contact Email', type 'email'
    soql_element :contact_email, 'ContactEmail'

    # Element for 'Contact Fax', type 'phone'
    soql_element :contact_fax, 'ContactFax'

    # Element for 'Internal Comments', type 'textarea'
    soql_element :internal_comments, 'Comments'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Engineering Req Number', type 'string'
    soql_element :engineering_req_number, 'EngineeringReqNumber__c'

    # Element for 'SLA Violation', type 'picklist'
    soql_element :sla_violation, 'SLAViolation__c'

    # Element for 'Product', type 'picklist'
    soql_element :product, 'Product__c'

    # Element for 'Potential Liability', type 'picklist'
    soql_element :potential_liability, 'PotentialLiability__c'

    # Element for 'Property', type 'picklist'
    soql_element :property, 'Property__c'
  end
end
