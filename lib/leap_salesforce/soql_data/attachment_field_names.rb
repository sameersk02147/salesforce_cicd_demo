# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Attachment mapped from SOQL Attachment
class Attachment < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Attachment ID', type 'id'
    soql_element :attachment_id, 'Id'

    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'

    # Element for 'Parent ID', type 'reference'
    soql_element :parent_id, 'ParentId'

    # Element for 'File Name', type 'string'
    soql_element :file_name, 'Name'

    # Element for 'Private', type 'boolean'
    soql_element :private, 'IsPrivate'

    # Element for 'Content Type', type 'string'
    soql_element :content_type, 'ContentType'

    # Element for 'Body Length', type 'int'
    soql_element :body_length, 'BodyLength'

    # Element for 'Body', type 'base64'
    soql_element :body, 'Body'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Description', type 'textarea'
    soql_element :description, 'Description'
  end
end
