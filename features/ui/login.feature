@ui
Feature: UI

    Scenario: Enter username
        When I open Salesforce
        Then I can enter a username

    Scenario: Hack cannot login
        Given I open Salesforce
        And I am a hacker
        When I try to login
        Then I am blocked
