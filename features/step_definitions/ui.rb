# frozen_string_literal: true

Given(/^I open Salesforce$/) do
  @browser.goto ENV['SCRATCH_INSTANCE_URL']
end

Then(/^I can enter a username$/) do
  on(LoginPage).username = 'Test User'
  sleep 2 # Put here just so video looks better
end

And(/^I am a hacker$/) do
  on(LoginPage) do |page|
    page.username = 'Hacker@gmail.com'
    page.password = 'Password'
  end
end

When(/^I try to login$/) do
  on(LoginPage).login
end

Then(/^I am blocked$/) do
  expect(on(LoginPage).error_message?).to be true
end
