FROM ruby:2.6

RUN apt update && apt -y install jq

 # Setup SFDX environment variables
ENV CLIURL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
ENV SFDX_AUTOUPDATE_DISABLE=false
ENV SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
ENV SFDX_DOMAIN_RETRY=300
ENV SFDX_DISABLE_APP_HUB=true
ENV SFDX_LOG_LEVEL=DEBUG
ENV ROOTDIR=force-app/main/default/
ENV TESTLEVEL=RunLocalTests
ENV SCRATCH_ORG_ALIAS=DreamHouse_Scratch
ENV PACKAGEVERSION=""
 # Install Salesforce CLI
RUN mkdir sfdx
RUN wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
RUN "./sfdx/install"
ENV export PATH=./sfdx/$(pwd):$PATH
 # Output CLI version and plug-in information
RUN sfdx --version
RUN sfdx plugins --core

# Install ruby gems so save time in pipeline
RUN mkdir /mysuite
WORKDIR /mysuite
COPY Gemfile /mysuite/Gemfile
COPY Gemfile.lock /mysuite/Gemfile.lock
RUN gem install bundler
RUN bundle install
